module Sidebar exposing (view)

import Html exposing (text, a, div, h3, h4, p, ul, li)
import Html.Attributes exposing (class, href)
import Html.Events exposing (onClick)

showSellPassesButton onClickMsg =
    a [ class "action"
      , onClick onClickMsg
      ] [ text "Sell Passes" ]

view onClickMsg =
    div [ class "sidebar" ]
        [ div [ class "section sell-passes" ]
              [ h3 [] [ text "Sell Passes" ]
              , p [] [ text "Sell passes instead of tickets if you want your attendees to buy a certain amount of credits redeemable for multiple events (ie. film festivals)." ]
              , p [] [ showSellPassesButton onClickMsg ]
              ]
        , div [ class "section questions" ]
            [ h4 [] [ text "Hosting an Event?" ]
            , ul [] [ li [] [ text "Sell tickets" ]
                    , li [] [ text "Promote your events easily" ]
                    , li [] [ a [ href "https://www.universe.com/directpayments" ]
                                  [ text "Direct checkout" ]
                            , text " on your site"
                            ]
                    , li [] [ text "Manage attendees" ]
                    , li [] [ a [ href "https://www.universe.com/ticketmanager" ]
                                  [ text "Scan tickets" ]
                            , text " at the door"
                            ]
                    , li [] [ a [ href "https://www.universe.com/features" ]
                                  [ text "Learn More" ]
                            ]
                    ]
            ]
        ]

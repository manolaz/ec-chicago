ec-chicago.js: CreateListingPage.elm
	elm make $^ --output ec-chicago.js

style.css: styles/page.scss
	scss styles/page.scss style.css

all: ec-chicago.js style.css

clean:
	rm ec-chicago.js style.css

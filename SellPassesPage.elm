module SellPassesPage exposing (page)

import Html exposing (a, div, h1, p, text)
import Html.Attributes exposing (class)
import Html.Events exposing (onClick)

createPassesButton passesMsg =
    a [ class "action"
      , onClick passesMsg
      ] [ text "Create a Pass" ]

page passesMsg ticketsMsg =
    div [ class "page-sell-passes" ]
        [ h1 [] [ text "Create a Pass" ]
        , p [] [ text "A pass can be redeemed across multiple events. To get started, you will need to create at least one pass, set the number of credits per pass, then specify which events the pass can be redeemed for." ]
        , p [] [ text "This feature is recommended for advanced users only." ]
        , p [] [ createPassesButton passesMsg ]
        , p [] [ text "Not the best fit for your event? Go back to "
               , a [ class "action"
                   , onClick ticketsMsg
                   ] [ text "Sell Tickets" ]
               ]
        ]

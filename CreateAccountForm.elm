module CreateAccountForm exposing (..)

import Html exposing (Html, div, input, label, text)
import Html.Attributes exposing (class, name, for, id, placeholder, type')
import Html.Events exposing (onInput)
import List exposing (map)
import Forms exposing (FormField, simpleField, formField)

type UserAccountType = Individual | Organization


organizationName = simpleField "first-name" "Business or organization name"
firstName = simpleField "first-name" "First Name"
lastName = simpleField "last-name" "Last Name"
email =
    { idAndName = "email"
    , labelText = "Email"
    , required = True
    , placeholder = Just "example@example.com"
    , note = Just "Private: Your email is never shared"
    , inputType = Nothing
    }

password =
    { idAndName = "password"
    , labelText = "Password"
    , required = True
    , placeholder = Just "Minimum 6 characters"
    , note = Just "Your password must be at least 6 characters"
    , inputType = Just "password"
    }

formFields : UserAccountType -> List FormField
formFields userAccountType =
    case userAccountType of
        Individual ->
            [ firstName , lastName , email , password ]
        Organization ->
            [ organizationName , email , password ]

form : UserAccountType -> Html msg
form userAccountType =
    userAccountType
        |> formFields
        |> map formField
        |> div [ class "form-create-account" ]

main =
    form Individual

module Event exposing (Event, emptyEvent)

type alias Event =
    { title : String
    , description : String
    , ticketNotes : Maybe String
    , contactDetails : Maybe String
    , referralCode : Maybe String
    }

emptyEvent =
    { title = ""
    , description = ""
    , ticketNotes = Nothing
    , contactDetails = Nothing
    , referralCode = Nothing
    }

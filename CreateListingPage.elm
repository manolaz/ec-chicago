module CreateListingPage exposing (..)

import Html exposing (Html, div, h1, h2, h3, h4, p, a, text, ul, li, button)
import Html.Attributes exposing (class, href)
import Html.Events exposing (onClick)
import Html.App exposing (beginnerProgram)
import List exposing (append, map)
import CreateAccountForm exposing (UserAccountType)
import Sidebar
import SellPassesPage
import EventDescriptionForm
import Event exposing (Event, emptyEvent)

-- Components as Functions

-- Page
pageTitle model =
    let
       s = case model.saleType of
               TicketSale ->
                   "Post Your Event and Start Selling Tickets"
               PassSale ->
                   "Setup Passes to Start Selling"
    in
        h1 [] [ text s ]

pageSubtitle model =
    let
        s = case model.saleType of
                TicketSale ->
                    "It's absolutely free and takes just seconds to post."
                PassSale ->
                    "A pass can be redeemed to buy tickets across multiple events."
    in
        h2 [] [ text s ]

sectionHeading heading =
    div [ class "heading" ]
        [ h3 [] [ text heading ]]

sectionCreateAccount : Model -> Html Msg
sectionCreateAccount model =
    section "create-account"
        "Create An Account"
        [ CreateAccountForm.form model.userAccountType
        , p [] [ text "Sign up as a "
               , a [ class "action"
                   , onClick SwitchUserAccountType
                   ]
                   [ text "business or organization" ]
               ]
        , p [] [ text "By using Universe you agree to the "
               , a [ href "https://www.universe.com/terms" ]
                   [ text "Terms & Conditions" ]
               , text "." ]
        ]

sectionEventLocation : Model -> Html Msg
sectionEventLocation model =
    div [] []

sectionEventDateTime : Model -> Html Msg
sectionEventDateTime model =
    div [] []

sectionEventTicketTypes : Model -> Html Msg
sectionEventTicketTypes model =
    div [] []

sectionEventSocialDeals : Model -> Html Msg
sectionEventSocialDeals model =
    div [] []

sectionEventPromotion : Model -> Html Msg
sectionEventPromotion model =
    div [] []

sectionEventPaymentPrefs : Model -> Html Msg
sectionEventPaymentPrefs model =
    div [] []

section : String -> String -> List (Html msg) -> Html msg
section className heading body =
    div [ "section " ++ className |> class ]
        [ sectionHeading heading
        , div [ class "container" ] body
        ]

eventSection className heading form event =
    section className heading [ form event ]

saveBar =
    div [ class "section save-bar" ]
        [ div [ class "note" ]
              [ text "You can edit these details at any time." ]
        , div [ class "actions" ]
              [ button [ class "action action-save"
                       , onClick SaveListing
                       ]
                    [ text "Save and Continue" ]
              ]
        ]

-- Program (model, view, update)
type Msg = SwitchUserAccountType
         | ChangeSaleType SaleType
         | ShowSellPassesPage
         | SaveListing

type SaleType = TicketSale | PassSale

type alias Model =
    { userAccountType : UserAccountType
    , saleType : SaleType
    , showSellPassesPage : Bool
    , event : Event
    }

model =
    { userAccountType = CreateAccountForm.Individual
    , saleType = TicketSale
    , showSellPassesPage = False
    , event = emptyEvent
    }

view : Model -> Html Msg
view model =
    if model.showSellPassesPage then
        SellPassesPage.page (ChangeSaleType PassSale) (ChangeSaleType TicketSale)
    else
        div [ class "page-create-listing" ]
            [ div [ class "sections" ]
                  [ pageTitle model
                  , pageSubtitle model
                  , sectionCreateAccount model
                  , eventSection "event-description" "Describe Your Event" EventDescriptionForm.form model.event
                  , sectionEventLocation model
                  , sectionEventDateTime model
                  , sectionEventTicketTypes model
                  , sectionEventSocialDeals model
                  , sectionEventPromotion model
                  , sectionEventPaymentPrefs model
                  , saveBar
                  ]
            , Sidebar.view ShowSellPassesPage
            ]

update : Msg -> Model -> Model
update msg model =
    case msg of
        SwitchUserAccountType ->
            case model.userAccountType of
                CreateAccountForm.Individual -> { model | userAccountType = CreateAccountForm.Organization }
                CreateAccountForm.Organization -> { model | userAccountType = CreateAccountForm.Individual }
        ChangeSaleType saleType ->
            { model | saleType = saleType
            , showSellPassesPage = False
            }
        ShowSellPassesPage ->
            { model | showSellPassesPage = True }
        SaveListing ->
            model

main =
    beginnerProgram
        { model = model
        , view = view
        , update = update
        }

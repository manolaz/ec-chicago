module Forms exposing (FormField, formInput, formLabel, formField, simpleField, InputType(TextArea))

import Html exposing (Html, input, text, label, div, textarea)
import Html.Attributes exposing (id, class, name, type', placeholder, for)
import Maybe exposing (withDefault)

type InputType = Text | Password | TextArea

type alias FormField =
    { idAndName : String
    , inputType : Maybe InputType
    , labelText : String
    , required : Bool
    , placeholder : Maybe String
    , note : Maybe String
    }

mapInputType : Maybe InputType -> String
mapInputType inputType =
    case inputType of
        Nothing -> "text"
        Just Text -> "text"
        Just Password -> "password"
        Just TextArea -> ""

-- Functions for creating form fields
formInput : FormField -> Html msg
formInput formField =
    let
        attributes = [ id formField.idAndName
                     , class "field"
                     , name formField.idAndName
                     , withDefault "" formField.placeholder |> placeholder ]
        textareaAttrs = [ class "field ckeditor" ]
        inputAttrs = [ class "field"
                     , mapInputType formField.inputType |> type'
                     ]
    in
        case formField.inputType of
            Just TextArea ->
                textarea (attributes ++ textareaAttrs) []
            _ ->
                input (attributes ++ inputAttrs) []

formLabel : FormField -> Html msg
formLabel formField =
    div [ class "form-label" ]
        [ label [ for formField.idAndName  ]
              [ text formField.labelText ]
        ]

formField : FormField -> Html msg
formField formField =
    div [ class "field" ]
        [ formLabel formField
        , formInput formField
        , div [ class "note" ]
            [ text (withDefault "" formField.note) ]
        ]

simpleField : String -> String -> FormField
simpleField idAndName labelText =
    { idAndName = idAndName
    , labelText = labelText
    , required = False
    , placeholder = Nothing
    , note = Nothing
    , inputType = Nothing
    }

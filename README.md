# Event Creator Chicago

Licensed under the AGPL v3 or later.

Copyright (C) 2016 Rudolf Olah

## Universe API stuff

The API documentation for the Universe.com API is here: http://developers.universe.com/

The event creation API isn't quite documented fully, though there's an example
of an update event request in the docs, and we're going to be examining a real
request made on the website.

The tricky part is going to be setting up OAuth and logging in so that you can
login and then use this event creation page.
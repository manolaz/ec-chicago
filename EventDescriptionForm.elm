module EventDescriptionForm exposing (form)

import Html
import Html.Attributes
import Forms exposing (FormField, InputType(TextArea), formField)

titleField =
    { idAndName = "title"
    , inputType = Nothing
    , labelText = "Title"
    , required = True
    , placeholder = Just "My amazing event"
    , note = Nothing
    }

descriptionTextArea =
    { idAndName = "description"
    , inputType = Just TextArea
    , labelText = "Description"
    , required = True
    , placeholder = Nothing
    , note = Nothing
    }

ticketNotesTextArea =
    { idAndName = "ticket-notes"
    , inputType = Just TextArea
    , labelText = "Ticket Notes"
    , required = False
    , placeholder = Nothing
    , note = Just "Private: This note will be displayed on the ticket confirmation."
    }

contactDetailsField =
    { idAndName = "contact-details"
    , inputType = Nothing
    , labelText = "Contact Details"
    , required = False
    , placeholder = Just "Shown only to people who book"
    , note = Just "Private: Your contact details will only be shared with people who book"
    }

referralCodeField =
    { idAndName = "referral-code"
    , inputType = Nothing
    , labelText = "Referral Code"
    , required = False
    , placeholder = Nothing
    , note = Just "Did someone refer you to Universe and give you a referral code?"
    }

formFields =
    [ titleField
    , descriptionTextArea
    , ticketNotesTextArea
    , contactDetailsField
    , referralCodeField
    ]

form event =
    List.map formField formFields
     |> Html.div [ Html.Attributes.class "form-event-description" ]
